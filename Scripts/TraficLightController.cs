﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class TraficLightController : MonoBehaviour
{

    private GameObject player, playercamera;
    private splineMove2 myMove;
    private GameObject GreenLight;
    private GameObject RedLight;
    private splineMoveSingleCar singleCarMove;
    public GameObject SingleCar;

    void Start ()
    {
        player = GameObject.Find("FPSController");
        playercamera = GameObject.FindGameObjectWithTag("MainCamera");
        myMove = GameObject.Find("FPSController").GetComponent<splineMove2>();
        GreenLight = GameObject.Find("GreenLight");
        RedLight = GameObject.Find("RedLight");

        RedLight.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.red);   //Задаем свечение красному сигналу светофора
        RedLight.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
    }

    public void TraficController()
    {
        if (GameObject.Find("SingleCar"))
        {
            if (singleCarMove.pathContainer.name.Equals("SingleCarRoute"))
            {
                if (singleCarMove.currentPoint == 9)
                {
                    singleCarMove.ChangeSpeed(10);
                }
                if (singleCarMove.currentPoint == 10)
                {
                    singleCarMove.ChangeSpeed(5);
                }
                if (singleCarMove.currentPoint == 11)
                {
                    singleCarMove.ChangeSpeed(3);
                }
                if (singleCarMove.currentPoint == 12)
                {
                    singleCarMove.Pause();
                }
            }
        }
    }

    void Update ()
    {
        if (myMove.pathContainer.name.Equals("LonesomeRoad1"))
        {
            if (myMove.currentPoint == 128) //Дошли до пешеходного перехода
            {
                StartCoroutine(waitForGreenSignal());   //Запуск сценария светофора
                TraficController();
            }
        }
    }

    IEnumerator waitForGreenSignal()
    {
        yield return new WaitForSeconds(20);
        RedLight.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black); //Выключаем красный сигнал
        RedLight.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
        GreenLight.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.green);   //Включаем зеленый сигнал
        GreenLight.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
        SingleCar.SetActive(true);          //Пуск одиночной машины
        singleCarMove = GameObject.Find("SingleCar").GetComponent<splineMoveSingleCar>();
    }
}

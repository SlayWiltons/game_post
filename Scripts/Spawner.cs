﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using SWS;
using UnityEngine.UI;


public class Spawner : MonoBehaviour
{
    public GameObject[] cars;
    public float spawnWait;
    public float spawnMostWait;
    public float spawnLeastWait;
    public int startWait;
    public bool stop;

    int isCarSignal;
    public float minWaitForSignal = 0;
    public float maxWaitForSignal = 2;
    float waitForSignal;

    int randCar;
    int counterRed;
    int counterBlue;

    public float lifeTime;


    GameObject Car; //Генерируемый объект


    void Start ()
    {
        counterRed = 0;
        counterBlue = 0;
        StartCoroutine(waitSpawner());
	}
	
	void Update ()
    {
        spawnWait = Random.Range(spawnLeastWait, spawnMostWait);    //Время ожидание спавна следующей машины
    }

    IEnumerator waitSpawner()
    {
        yield return new WaitForSeconds(startWait);
        while (!stop)   //делаем справн постоянным
        {
            randCar = Random.Range(0, 6);
            waitForSignal = Random.Range(minWaitForSignal, maxWaitForSignal);
            Car = Instantiate(cars[randCar]);   //Спавним новый объект
            cars[randCar].SetActive(true);
            if (cars[randCar] == GameObject.Find("redCar"))
            {
                Destroy(Car, lifeTime); //Уничтожаем объект через определенное время
            }
            if (cars[randCar] == GameObject.Find("blueCar"))
            {
                Destroy(Car, lifeTime);
            }
            if (cars[randCar] == GameObject.Find("greenCar"))
            {
                Destroy(Car, lifeTime);
            }
            if (cars[randCar] == GameObject.Find("brownCar"))
            {
                Destroy(Car, lifeTime);
            }
            if (cars[randCar] == GameObject.Find("grayCar"))
            {
                Destroy(Car, lifeTime);
            }
            if (cars[randCar] == GameObject.Find("blackCar"))
            {
                Destroy(Car, lifeTime);
            }
            yield return new WaitForSeconds(spawnWait);
        }
    }
}

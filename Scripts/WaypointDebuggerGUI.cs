﻿using UnityEngine;
using SWS;
using UnityEngine.SceneManagement;

public class WaypointDebuggerGUI : MonoBehaviour
{
	private splineMove2 myMove;
    private GameObject player;
    private GameObject playercamera;
    private GameObject TaxiSpawnersInfo;
    private GameObject TaxiCounterInfo;
    private GameObject AlarmInfo;

    private GameObject AmbulanceSound;

    private static GeneralConfig config;

    bool carsOffFlag;
    bool alarmFlag;

    void Start ()
    {
        TaxiCounterInfo = GameObject.Find("TaxiCounter");
        TaxiSpawnersInfo = GameObject.Find("TaxiSpawners");
        AlarmInfo = GameObject.Find("AlarmInfo");
        AmbulanceSound = GameObject.Find("AmbulanceSoundSource");
        player = GameObject.FindGameObjectWithTag("Player");
        myMove = gameObject.GetComponent<splineMove2>();
		myMove.setStartedPath ("HomeToMarket1");
        carsOffFlag = false;
        alarmFlag = false;
    }

	void OnGUI()
    {
        GUI.Box(new Rect(1730, 10, 170, 210), "Меню");
        if (GUI.Button(new Rect(1740, 40, 150, 20), "Начало"))
        {
            Application.LoadLevel("_Stage1Build3-39");
        }
        if (GUI.Button(new Rect(1740, 70, 150, 20), "Начало движения"))
        {
            myMove.SetPath(WaypointManager.Paths["LonesomeRoad1"]);
            myMove.GoToWaypoint(0);
        }
        if (GUI.Button(new Rect(1740, 100, 150, 20), "Сирена"))
        {
			myMove.SetPath (WaypointManager.Paths["LonesomeRoad1"]);
			myMove.GoToWaypoint (49);
            AmbulanceSound.GetComponent<AudioSource>().Stop();
        }
        if (GUI.Button(new Rect(1740, 130, 150, 20), "Сирена2"))
        {
            myMove.SetPath(WaypointManager.Paths["LonesomeRoad1"]);
            myMove.GoToWaypoint(122);
            AmbulanceSound.GetComponent<AudioSource>().Stop();
        }
        if (GUI.Button (new Rect (1740, 160, 150, 20), "Пешеходный переход"))
        {
            alarmFlag = true;
            TaxiCounterInfo.SetActive(false);
            TaxiSpawnersInfo.SetActive(false);            
            myMove.SetPath (WaypointManager.Paths["LonesomeRoad1"]); 
            myMove.GoToWaypoint (127);
            AmbulanceSound.GetComponent<AudioSource>().Stop();
            carsOffFlag = true;            
        }
        if (GUI.Button(new Rect(1740, 190, 150, 20), "После перехода"))
        {
            alarmFlag = true;
            TaxiCounterInfo.SetActive(false);
            TaxiSpawnersInfo.SetActive(false);            
            myMove.SetPath(WaypointManager.Paths["ToPost"]);
            myMove.GoToWaypoint(0);
            AmbulanceSound.GetComponent<AudioSource>().Stop();
            carsOffFlag = true;
        }
    }

    public void AlarmOff()
    {
        if (alarmFlag == true)
        {
            AmbulanceSound.GetComponent<AudioSource>().Stop();
            AlarmInfo.SetActive(false);
        }
    }

    public void HideCars()  //Убираем машины, в случае, если переместились к переходу через меню
    {
        if (carsOffFlag == true)
        {
            if (GameObject.Find("redCar(Clone)"))
            {
                Destroy(GameObject.Find("redCar(Clone)"), 0.1f);
            }
            if (GameObject.Find("redCar2(Clone)"))
            {
                Destroy(GameObject.Find("redCar2(Clone)"), 0.1f);
            }
            if (GameObject.Find("blueCar(Clone)"))
            {
                Destroy(GameObject.Find("blueCar(Clone)"), 0.1f);
            }
            if (GameObject.Find("blueCar2(Clone)"))
            {
                Destroy(GameObject.Find("blueCar2(Clone)"), 0.1f);
            }
            if (GameObject.Find("taxiCar(Clone)"))
            {
                Destroy(GameObject.Find("taxiCar(Clone)"), 0.1f);
            }
            if (GameObject.Find("redCar"))
            {
                GameObject.Find("redCar").SetActive(false);
            }
            if (GameObject.Find("redCar2"))
            {
                GameObject.Find("redCar2").SetActive(false);
            }
            if (GameObject.Find("blueCar"))
            {
                GameObject.Find("blueCar").SetActive(false);
            }
            if (GameObject.Find("blueCar2"))
            {
                GameObject.Find("blueCar2").SetActive(false);
            }
            if (GameObject.Find("taxiCar"))
            {
                GameObject.Find("taxiCar").SetActive(false);
            }
        }
    }

    void Update ()
    {
        HideCars();        
    }    
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SWS;
using TurnTheGameOn.Timer;
using System;

[RequireComponent(typeof(AudioSource))]
public class WaypointContainersAnalyzer : MonoBehaviour 
{
    #region Values Description

    private splineMove2 myMove;
    public GameObject CarsLeft;
    public GameObject CarsRight;    
	private mqttTest mqttdata;
	private HttpDataScript httpdata;
	private GameObject player, playercamera;
	private static GeneralConfig config;
    private bool soundflag;
    int alarmdetect = 0;
    public GameObject AmbulanceSound;
    public GameObject RoadSounds;
    public GameObject AmbulanceSoundInfo;

    public GameObject Spawner1;
    public GameObject Spawner2;

    public GameObject Instructions;

    public GameObject taxiCarsCounter;
    int taxiCount = 0;

    private int tempStep = 0;
    #endregion

    #region voidStart
    void Start () 
	{
        player = GameObject.Find ("FPSController");
		playercamera = GameObject.FindGameObjectWithTag ("MainCamera");
		myMove = GameObject.Find ("FPSController").GetComponent<splineMove2> ();

 		/* Mqtt Data */
		mqttdata = GameObject.Find ("FPSController").GetComponent<mqttTest>();
		config = new GeneralConfig ();

		/* Http Data */
		httpdata = GameObject.Find ("FPSController").GetComponent<HttpDataScript>();
		myMove.setStartedPath ("FromHomeToStairs");
        soundflag = false;
    }
    #endregion


    public void containerManualFromMyMove(PathManager pathContainer, int index)
    {

        // Home To Stairs
        #region From_Home_To_Stairs
        if (pathContainer.name.Equals ("FromHomeToStairs")) 
		{
            myMove.ChangeSpeed(0.8f);
            if (index == 0) //Включаем спавны машин
            {
                Spawner1.SetActive(true);
                Spawner2.SetActive(true);
            }
            if (index == 7)
            {
                myMove.SetPath(WaypointManager.Paths["FromStairsToCorner"]);
                myMove.ChangeSpeed(1.0f);
            }
		}
        #endregion
        #region From_Stairs_To_Corner
        if (pathContainer.name.Equals("FromStairsToCorner"))
        {
            if (index == 3) //Запускаем проезжей части
            {
                myMove.SetPath(WaypointManager.Paths["LonesomeRoad1"]);
                RoadSounds.GetComponent<AudioSource>().Play();
            }
        }
        #endregion

        if (pathContainer.name.Equals("LonesomeRoad1"))
        {
            if (index == 20)    //Запускаем тестовую сирену
            {
                AmbulanceSound.GetComponent<AudioSource>().Stop();
                AmbulanceSound.SetActive(true);
                AmbulanceSound.GetComponent<AudioSource>().Play();
            }
            if (index == 50)    //Запускаем первую сирену
            {
                AmbulanceSound.GetComponent<AudioSource>().Stop();
                AmbulanceSoundInfo.SetActive(true);
                AmbulanceSound.GetComponent<AudioSource>().Play();
                Debug.Log("Сирена 1!");
            }
            if (index == 123)   //Запускаем вторую сирену
            {
                AmbulanceSound.GetComponent<AudioSource>().Stop();
                AmbulanceSoundInfo.SetActive(true);
                AmbulanceSound.GetComponent<AudioSource>().Play();
                Debug.Log("Сирена 2!");
            }
            if (index == 100)   //Отключаем левый спавнер
            {
                CarsLeft.SetActive(false);
                Debug.Log("Left spawner disabled");
            }
            if (index == 123)   //Отключаем правый спавнер
            {
                CarsRight.SetActive(false);
                Debug.Log("Right spawner disabled");
            }
            if (index == 125)   //Отключаем звук дороги
            {
                StartCoroutine(RoadSoundStopping());
                Debug.Log("Road sound off");
            }

        }
        
        if (pathContainer.name.Equals("LonesomeRoad1")) 
        {
            if (index == 128)   //Тормозим пациента у пешеходного перехода
            {
                myMove.Pause();
            }
            if (index == 130)
            {
                myMove.SetPath(WaypointManager.Paths["ToPost"]);
            }
        }

        if (pathContainer.name.Equals("ToPost"))
        {
            //Останавливаемся у препятствия на дороге
            if (index == 8)
            {
                myMove.Pause();
            }
            if (index == 23)    //Подъем на почту
            {
                myMove.SetPath(WaypointManager.Paths["PostEntrance"]);
            }
        }

        if (pathContainer.name.Equals("Trash"))
        {
            if (index == 10)
            {
                myMove.SetPath(WaypointManager.Paths["ToPost"]);
                myMove.GoToWaypoint(10);
                myMove.Resume();
            }
        }
    }

    public void AmbulanceSoundController()
    {
        if (AmbulanceSound.GetComponent<AudioSource>().isPlaying)   //Засчитываем реакцию на сирену (пока сирена активна)
        {
            if (Input.GetMouseButtonDown(0))
            {
                alarmdetect++;
            }
            AmbulanceSoundInfo.GetComponent<Text>().text = "Сирена: " + alarmdetect;
        }
    }

    // Step analyze and lock
    #region containerMoveStepAnalyzer
    public void containerMoveStepAnalyzer() 
	{
		if (mqttdata.step == 1 && tempStep == 0 && myMove.getMoveLock () == false) 
		{
			tempStep = 1;
			myMove.Resume();
		}
		if (mqttdata.step == 0 && tempStep == 1 && myMove.getMoveLock () == false) 
		{
			tempStep = 0;
		}

		if (mqttdata.step == 1 && myMove.getMoveLock () == true) 
		{

		}

	}
    #endregion

    #region TaxiCarsCounter
    public void TaxiCarsCounter()   //Подсчет машин такси
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            taxiCount++;
            taxiCarsCounter.GetComponent<Text>().text = "Подсчитано машин Такси: " + taxiCount.ToString();
        }
    }
    #endregion

    IEnumerator RoadSoundStopping()
    {
        while (RoadSounds.GetComponent<AudioSource>().volume != 0.0f)
        {
            yield return new WaitForSeconds(0.5f);
            RoadSounds.GetComponent<AudioSource>().volume -= 0.1f;
            StopCoroutine(RoadSoundStopping());
        }
    }

	IEnumerator continueToGo()
	{
		yield return new WaitForSeconds(1);
		myMove.Resume ();
		StopCoroutine (continueToGo());
	}

    void Update()
    {
        AmbulanceSoundController();
        containerMoveStepAnalyzer();
        TaxiCarsCounter();
        ShowInstructions();
        PauseAll();
        if (player.GetComponent<splineMove2>().pathContainer.name == "ToPost")
        {
            if (myMove.currentPoint == 8)   //Этап с препятствием на тротуаре
            {
                if (Input.GetKeyDown(KeyCode.LeftArrow)) // Можем или обойти препятствие...
                {
                    myMove.SetPath(WaypointManager.Paths["Trash"]);
                    myMove.GoToWaypoint(8);
                    myMove.Resume();
                    Debug.Log("Go left");
                }
                if (Input.GetKeyDown(KeyCode.UpArrow))  //... или перешагнуть его
                {
                    myMove.Resume();
                    Debug.Log("Go forward");
                }
            }
        }
    }

    void ShowInstructions() //Вывод общих инструкций на экран
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (!Instructions.activeSelf)
            {
                Instructions.SetActive(true);
            }
            else
            {
                Instructions.SetActive(false);
            }
        }
    }

    void PauseAll()
    {
        if (Input.GetKeyDown(KeyCode.Pause))
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
        }
    }
}

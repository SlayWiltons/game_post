﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using SWS;
using UnityEngine.UI;


public class Spawner2 : MonoBehaviour
{
    public GameObject[] cars;
    public float spawnWait;
    public float spawnMostWait;
    public float spawnLeastWait;
    public int startWait;
    public bool stop;

    public GameObject taxicar;

    int randCar;
    int counterSpawnTaxi;
    int taxiId;

    int carsquantity = 0;


    public float lifeTime;

    private GameObject TaxiCarSpawner;

    //Точки спавна машин такси
    float randsp1 = 0;
    float randsp2 = 0;
    float randsp3 = 0;


    GameObject Car;

    void Start ()
    {
        taxiId = 0;
        counterSpawnTaxi = 0;
        StartCoroutine(waitSpawner());
        TaxiCarSpawner = GameObject.Find("TaxiSpawners");
        randsp1 = Random.Range(10, 20); //Задаем какими по счету будут заспавнены машины такси
        randsp2 = Random.Range(60, 75);
        randsp3 = Random.Range(120, 130);
    }
	
	void Update ()
    {
        spawnWait = Random.Range(spawnLeastWait, spawnMostWait);    //время ожидания спавна следующей машины 
        Debug.Log("Spawn 1 = " + randsp1);
        Debug.Log("Spawn 2 = " + randsp2);
        Debug.Log("Spawn 3 = " + randsp3);
    }

    IEnumerator waitSpawner()
    {
        yield return new WaitForSeconds(startWait);
        while (!stop)
        {
            {
                randCar = Random.Range(0, 7);
                Car = Instantiate(cars[randCar]);
                cars[randCar].SetActive(true);
                if (cars[randCar] == GameObject.Find("redCar2"))
                {
                    carsquantity++;
                    Destroy(Car, lifeTime);
                    Debug.Log("Cars quantity: " + carsquantity);
                }
                if (cars[randCar] == GameObject.Find("blueCar2"))
                {
                    carsquantity++;
                    Destroy(Car, lifeTime);
                    Debug.Log("Cars quantity: " + carsquantity);
                }
                if (cars[randCar] == GameObject.Find("greenCar2"))
                {
                    carsquantity++;
                    Destroy(Car, lifeTime);
                    Debug.Log("Cars quantity: " + carsquantity);
                }
                if (cars[randCar] == GameObject.Find("brownCar2"))
                {
                    carsquantity++;
                    Destroy(Car, lifeTime);
                    Debug.Log("Cars quantity: " + carsquantity);
                }
                if (cars[randCar] == GameObject.Find("grayCar2"))
                {
                    carsquantity++;
                    Destroy(Car, lifeTime);
                    Debug.Log("Cars quantity: " + carsquantity);
                }
                if (cars[randCar] == GameObject.Find("blackCar2"))
                {
                    carsquantity++;
                    Destroy(Car, lifeTime);
                    Debug.Log("Cars quantity: " + carsquantity);
                }
                if (cars[randCar] == GameObject.Find("taxiCar"))
                {
                    carsquantity++;
                    Destroy(Car, lifeTime);
                    counterSpawnTaxi++;
                    TaxiCarSpawner.GetComponent<Text>().text = "Количество машин Такси: " + counterSpawnTaxi.ToString();
                }
            }
            yield return new WaitForSeconds(spawnWait);
        }
    }
}
